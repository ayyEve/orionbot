use crate::db::schema::invites;
use diesel::prelude::*;

use crate::db::*;

use nebbot_utils::types::TextU64;

use chrono::prelude::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[primary_key(code)]
#[table_name = "invites"]
pub struct DbInvite {
    pub code: String,
    pub inviter: TextU64,
    pub uses: i64,
    pub guild_id: TextU64,
    pub max_age: i64,
    pub max_uses: i64,
    pub created_at: DateTime<Utc>,
    pub temporary: bool,
}

impl From<serenity::model::invite::RichInvite> for DbInvite {
    fn from(invite: serenity::model::invite::RichInvite) -> DbInvite {
        DbInvite {
            code: invite.code,
            inviter: TextU64(invite.inviter.id.0),
            guild_id: TextU64(invite.guild.unwrap().id.0),
            uses: invite.uses as i64,
            max_age: invite.max_age as i64,
            max_uses: invite.max_uses as i64,
            created_at: Utc::now(),
            temporary: invite.temporary,
        }
    }
}

impl Db {
    pub async fn add_invite(&self, invite: DbInvite) -> QueryResult<()> {
        diesel::insert_into(invites::table).values(&invite).execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn remove_invite(&self, code: String) -> QueryResult<()> {
        diesel::delete(invites::table)
            .filter(invites::code.eq(code))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn clear_invites(&self, guild_id: u64) -> QueryResult<()> {
        diesel::delete(invites::table)
            .filter(invites::guild_id.eq(TextU64(guild_id)))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn get_all_invites(&self, guild_id: u64) -> QueryResult<Vec<DbInvite>> {
        invites::table.filter(invites::guild_id.eq(TextU64(guild_id)))
            .load(&*self.conn.lock().await)
    }

    pub async fn get_invite(&self, code: String) -> QueryResult<Option<DbInvite>> {
        invites::table.find(code)
            .first(&*self.conn.lock().await).optional()
    }
}