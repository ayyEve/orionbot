use serenity:: {
    cache::Cache,
    http::client::Http,
    framework::standard::CommandResult
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use futures_util::StreamExt;

use std::sync::Arc;

use crate::db::*;
use crate::cache::*;
use nebbot_utils::prelude::*;

pub async fn check_richest(ctx: &Context, user: DbUser, lost_money: bool) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let mut bot_cache_lock = bot_cache.lock().await;

    let guild_id = GuildId(user.guild_id.0);

    match bot_cache_lock.richest.get(&user.guild_id.0) {
        Some((u, n)) => {
            let cached_user_id = *u;
            let cached_nuggets = *n;

            if lost_money {
                // If the user losing money is not the current richest user,
                // then the richest user remains unchangd.
                if user.discord_id.0 != cached_user_id { return Ok(()); }

                let richest_role = match database.get_guild(user.guild_id.0).await? {
                    Some(DbGuild { richest_role: Some(role), .. }) => RoleId(role.0),
                    _ => return Ok(()),
                };

                let richest = match database.select_top_10_users(user.guild_id.0).await?.first() {
                    Some(u) => u.clone(),
                    None => return Ok(()),
                };

                // If the richest user is still the richest user,
                // only the cached nuggets need to be updated.
                bot_cache_lock.richest.insert(richest.guild_id.0, (richest.discord_id.0, richest.nuggets));

                // Otherwise, the role also needs to be updated.
                if richest.discord_id.0 != cached_user_id { 
                    let mut member = guild_id.member(&ctx.http, user.discord_id.0).await?;
                    member.remove_role(&ctx.http, richest_role).await?;

                    let mut member = guild_id.member(&ctx.http, richest.discord_id.0).await?;
                    member.add_role(&ctx.http, richest_role).await?;
                }
            }

            else {
                // If the current user is not the richest,
                // and the user does not have more nuggets than the richest,
                // then the richest user remains unchanged.
                if cached_user_id != user.discord_id.0 && cached_nuggets > user.nuggets { return Ok(()); }

                // If the current user is already the richest,
                // only the cached nuggets need to be updated.
                bot_cache_lock.richest.insert(user.guild_id.0, (user.discord_id.0, user.nuggets));
                
                if cached_user_id != user.discord_id.0 {
                    // Otherwise, the current user is now the richest,
                    // and the role also needs to be updated.

                    let richest_role = match database.get_guild(user.guild_id.0).await? {
                        Some(DbGuild { richest_role: Some(role), .. }) => RoleId(role.0),
                        _ => return Ok(()),
                    };

                    let mut member = guild_id.member(&ctx.http, cached_user_id).await?;
                    member.remove_role(&ctx.http, richest_role).await?;

                    let mut member = guild_id.member(&ctx.http, user.discord_id.0).await?;
                    member.add_role(&ctx.http, richest_role).await?;
                }
            }
        }
        None => {
            // If there are no cached richest users, cache the richest user.

            let richest_role = match database.get_guild(user.guild_id.0).await? {
                Some(DbGuild { richest_role: Some(role), .. }) => RoleId(role.0),
                _ => return Ok(()),
            };

            let richest = match database.select_top_10_users(user.guild_id.0).await?.first() {
                Some(u) => u.clone(),
                None => return Ok(()),
            };

            bot_cache_lock.richest.insert(richest.guild_id.0, (richest.discord_id.0, richest.nuggets));

            let mut member = guild_id.member(&ctx.http, richest.discord_id.0).await?;
            member.add_role(&ctx.http, richest_role).await?;
        }
    }

    Ok(())
}

pub async fn check_longest_spinner(ctx: &Context, spin: DbSpinner) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let mut bot_cache_lock = bot_cache.lock().await;

    let guild_id = GuildId(spin.guild_id.0);

    // This function is called only when a given user has a new top spin.

    match bot_cache_lock.longest_spin.get(&spin.guild_id.0) {
        Some((u, d)) => {
            let cached_user_id = *u;
            let cached_duration = *d;

            // If the current user was not the top spinner,
            // and their spin was not longer than the top spinner,
            // then the longest spinning user remains unchanged.
            if cached_user_id != spin.discord_id.0 && cached_duration > spin.duration { return Ok(()); }

            let longest_spin_role = match database.get_guild(spin.guild_id.0).await? {
                Some(DbGuild { longest_spin_role: Some(role), .. }) => RoleId(role.0),
                _ => return Ok(()),
            };

            // If the current user is already the top spinner,
            // only the cached duration needs to be updated.
            bot_cache_lock.longest_spin.insert(spin.guild_id.0, (spin.discord_id.0, spin.duration));

            if cached_user_id != spin.discord_id.0 {
                // Otherwise, there current user is now the top spinner,
                // and the role also needs to be updated.

                let mut member = guild_id.member(&ctx.http, cached_user_id).await?;
                member.remove_role(&ctx.http, longest_spin_role).await?;

                let mut member = guild_id.member(&ctx.http, spin.discord_id.0).await?;
                member.add_role(&ctx.http, longest_spin_role).await?;
            }
        },
        None => {
            // If there are no cached longest spinners, cache the longest spinner.

            let longest_spin_role = match database.get_guild(spin.guild_id.0).await? {
                Some(DbGuild { longest_spin_role: Some(role), .. }) => RoleId(role.0),
                _ => return Ok(()),
            };

            let longest_spinner = match database.select_top_10_spinners(spin.guild_id.0).await?.first(){
                Some(s) => s.clone(),
                None => return Ok(()),
            };

            bot_cache_lock.longest_spin.insert(longest_spinner.guild_id.0, (longest_spinner.discord_id.0, longest_spinner.duration));

            let mut member = guild_id.member(&ctx.http, longest_spinner.discord_id.0).await?;
                member.add_role(&ctx.http, longest_spin_role).await?;
        }
    }

    Ok(())
}

pub async fn cache_roles(http: Arc<Http>, cache: Arc<Cache>, db: Arc<Db>, bot_cache: Arc<Mutex<BotCache>>) {
    info!("Caching roles.");
    for guild_id in cache.guilds().await {
        if let Err(e) = cache_roles_for(http.clone(), db.clone(), bot_cache.clone(), guild_id).await {
            error!("Error caching commands for guild {} -- {:?}", guild_id.0, e);
        }
    }

    info!("Finished caching roles.");
}

pub async fn cache_roles_for(http: Arc<Http>, db: Arc<Db>, bot_cache: Arc<Mutex<BotCache>>, guild_id: GuildId) -> CommandResult {
    let db_guild = match db.get_guild(guild_id.0).await? {
        Some(g) => g,
        None => return Ok(()),
    };

    let mut richest = None;

    if let Some(TextU64(role)) = db_guild.richest_role {
        if let Some(u) = db.select_top_10_users(guild_id.0).await?.first() {
            bot_cache.lock().await.richest.insert(guild_id.0, (u.discord_id.0, u.nuggets));
    
            let mut member = guild_id.member(&http, &UserId(u.discord_id.0)).await.unwrap();
    
            member.add_role(&http, role).await?;

            richest = Some((role, Some(u.discord_id.0)));
        }
        else {
            richest = Some((role, None));
        }
    }

    let mut longest_spinner = None;

    if let Some(TextU64(role)) = db_guild.longest_spin_role {       
        if let Some(s) = db.select_top_10_spinners(guild_id.0).await?.first() {
            bot_cache.lock().await.longest_spin.insert(guild_id.0, (s.discord_id.0, s.duration));
    
            let mut member = guild_id.member(&http, &UserId(s.discord_id.0)).await.unwrap();
    
            member.add_role(&http, role).await?;

            longest_spinner = Some((role, Some(s.discord_id.0)));
        }
        else {
            longest_spinner = Some((role, None));
        }
    }

    if richest.is_some() || longest_spinner.is_some() {
        let mut ignore = Vec::new();
        let mut clear = Vec::new();

        if let Some((role, user_option)) = richest {
            clear.push(role);

            if let Some(user) = user_option {
                ignore.push(user);
            }
        }

        if let Some((role, user_option)) = longest_spinner {
            clear.push(role);

            if let Some(user) = user_option {
                ignore.push(user);
            }
        }

        let clear: Vec<RoleId> = clear.into_iter().map(|x| RoleId(x)).collect();

        let mut counter = 0;

        let mut all_members = guild_id.members_iter(&http).boxed();
        while let Some(member_result) = all_members.next().await {
            let mut member = member_result?;
    
            if ignore.contains(&member.user.id.0) { continue; }

            if !member.roles.iter().any(|x| clear.contains(x)) { continue; }
        
            member.remove_roles(&http, clear.as_slice()).await?;
            
            counter += 1;
            debug!("Removed roles from user {} in guild {}", member.user.id.0, guild_id.0);
        }

        if counter > 0 {
            info!("Removed roles from {} users.", counter);
        }
    }

    info!("Successfully cached roles for `{}` ({}).",
        guild_id.to_partial_guild(&http).await.expect("Failed to get guild data.").name,
        guild_id.0);

    Ok(())
}