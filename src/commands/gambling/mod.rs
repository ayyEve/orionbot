mod blackjack; use blackjack::*;
mod slots; use slots::*;

use serenity::framework::standard::macros::group;

#[group]
#[commands(blackjack, slots)]
struct Gambling;

pub const SLOT_PRICE: i64 = 75;
