use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use crate::db::DbUser;

use nebbot_utils::{
    discord::{ MatchDiscordUser, match_discord_user },
};

use diesel::SaveChangesDsl;

#[command]
#[description("Pay another user some Nuggets! :gem:")]
#[usage("pay <user> <amount>")]
#[only_in(guilds)]
async fn pay(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let user = match match_discord_user(&args, &ctx, discord_id).await {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Pay")
                .description("You cannot pay a bot!")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Pay")
                .description("You cannot pay yourself!")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        },
        MatchDiscordUser::InvalidForm(_) | MatchDiscordUser::NoArgument |
        MatchDiscordUser::NoUserFound => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Pay")
                .description("Invalid User Argument")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        },
    };

    let amount = match args.advance().current() {
        Some(s) => {
            match s.parse::<i64>() {
                Ok(i) => i,
                Err(_) => {
                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                        .title("Pay")
                        .description("Invalid amount")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    )).await?;
        
                    return Ok(());
                }
            }
        },

        None => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Pay")
                .description("Invalid amount")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        }
    };

    if amount < 0 {
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Pay")
            .description("Nice try, thief!")
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    if amount == 0 {
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Pay")
            .description("Invalid amount")
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    let host = match database.get_user(discord_id, guild_id).await? {
        Some(u) => u,
        None => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Pay")
                .description("You don't have enough Nuggets! :gem:")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(())
        }
    };

    if host.nuggets < amount {
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Pay")
            .description("You don't have enough Nuggets! :gem:")
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    let other = match database.get_user(user.id.0, guild_id).await? {
        Some(other) => {
            let mut update = other.update();
            update.nuggets = Some(other.nuggets + amount);
            update.save_changes(&*database.conn.lock().await)?
        },
        None => {
            let mut default = DbUser::default(user.id.0, guild_id);

            default.nuggets += amount;

            database.add_user(default.clone()).await?;

            default
        }
    };

    let mut update = host.update();
    update.nuggets = Some(host.nuggets - amount);
    let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Pay")
        .description(format!("Successfully paid {} Nuggets! :gem: to {}", amount, user.mention()))
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    crate::system::roles::check_richest(&ctx, other, false).await
}