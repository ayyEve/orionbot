use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::prelude::*;
use chrono::Duration;

use crate::db::*;
use diesel::SaveChangesDsl;

use nebbot_utils::time::human_readable;

#[command]
#[description("Gain 20 Nuggets! :gem: every hour")]
#[usage("hourly")]
#[only_in(guilds)]
async fn hourly(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let user = match database.get_user(discord_id, guild_id).await? {
        Some(user) => {
            let duration = Utc::now() - user.last_hourly;

            if duration <= Duration::hours(1) {
                let time_next = Duration::hours(1) - duration;

                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                    .title("Hourly")
                    .description(format!("You need to wait {}",
                        human_readable(time_next)))
                    .colour(crate::EMBED_COLOUR)
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                )).await?;

                return Ok(());
            }

            let mut update = user.update();

            update.nuggets = Some(user.nuggets + super::HOURLY);
            update.last_hourly = Some(Utc::now());

            update.save_changes(&*database.conn.lock().await)?
        }

        None => {
            let mut default = DbUser::default(discord_id, guild_id);

            default.nuggets += super::HOURLY;
            default.last_hourly = Utc::now();

            database.add_user(default.clone()).await?;

            default
        }
    };

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Hourly")
        .description(format!("Gained {} Nuggets! :gem: You now have {} Nuggets! :gem:", super::HOURLY, user.nuggets))
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    crate::system::roles::check_richest(&ctx, user, false).await
}