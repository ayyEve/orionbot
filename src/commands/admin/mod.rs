use serenity::framework::standard::macros::group;

mod refresh_roles; use refresh_roles::*;

#[group]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[owner_privilege(true)]
#[commands(refreshroles)]
struct Admin;