use serenity::framework::standard::macros::group;

mod commands; use commands::*;
mod general; use general::*;

#[group]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[owner_privilege(true)]
#[prefix("config")]
#[commands(commands, server)]
#[default_command(server)]
struct Configuration;