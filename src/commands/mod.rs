mod general; pub use general::*;
mod economy; pub use economy::*;
mod gambling; pub use gambling::*;
mod config; pub use config::*;
mod help; pub use help::*;
mod admin; pub use admin::*;

use serenity::framework::standard::{ CommandResult, macros::hook };
use serenity::prelude::*;
use serenity::model::prelude::*;

use crate::cache::*;

pub const IGNORE_WHITELIST: &'static[&'static str] = &[
    "server",
    "commands",
    "help"
];

#[hook]
pub async fn after(_ctx: &Context, _msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(_) => (),
        Err(e) => {
            error!("Command {} -- {:?}", command_name, e);
            return;
        },
    }
}

#[hook]
pub async fn before(ctx: &Context, msg: &Message, command_name: &str) -> bool {
    let guild_id = match msg.guild_id {
        Some(g) => g.0,
        None => return true,
    };

    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");

    let is_enabled = match database.is_command_enabled(guild_id, msg.channel_id.0, command_name).await {
        Ok(b) => b,
        Err(e) => {
            error!("Before hook, database error: {:?}", e);
            false
        }
    };

    is_enabled
}
