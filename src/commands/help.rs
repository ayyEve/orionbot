nebbot_utils::auto_help!(
    Orion,
    crate::EMBED_COLOUR,
    |ctx: Context, msg: Message| {
        Box::pin(async move {
            let data = ctx.data.read().await;
            let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

            let guild_id = match msg.guild_id {
                Some(g) => g,
                None => return crate::DEFAULT_PREFIX.to_owned(),
            };

            let guild = match database.get_guild(guild_id.0).await.expect("Failed to get guild for prefix from database.") {
                Some(g) => g,
                None => return crate::DEFAULT_PREFIX.to_owned(),
            };

            guild.custom_prefix.unwrap_or(crate::DEFAULT_PREFIX.to_owned())
        })
    }
);