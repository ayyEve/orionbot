-- Your SQL goes here
CREATE TABLE spinners (
  discord_id text NOT NULL,
  channel_id text NOT NULL,
  knocked_user_id text NOT NULL,
  duration bigint NOT NULL,
  guild_id text NOT NULL,
  PRIMARY KEY(discord_id, guild_id)
);