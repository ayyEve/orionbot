# Orion Bot

Orion Bot is a general purpose and fun discord bot for the Nebula Hangout server.

[[_TOC_]]

## Installation

### Bot Settings

The bot expects a file called `.env` in the root directory to store the discord token and Postgres configuration options.

**Example:**

```env
DISCORD_TOKEN=jkdfkdjfd
DATABASE_URL=postgres://postgres:password@localhost:5432/orionbot
```

## Database Setup

Install diesel cli

```shell
cargo install diesel_cli --no-default-features --features postgres
```

Run the migrations in the project's root directory

```shell
diesel setup
```

Note: this uses the `DATABASE_URL` from the `.env` file

## Build and Run

Use cargo to build and run the project

```shell
cargo run
```
